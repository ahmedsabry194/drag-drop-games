﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalWordGame
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private AiryUIAnimationManager winPanel;

        private int requiredAnswers = 3;
        private int currentCorrectAnswers = 0;

        public static GameManager Instance;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        public void OnCorrectAnswer()
        {
            currentCorrectAnswers++;
            AudioManager.Instance.PlayAudio("correct");

            if (currentCorrectAnswers >= 3)
            {
                Destroy(LetterSelector.Instance.gameObject);

                CoroutineHandler.Instance.PlayCoroutineWithTime(() =>
                {
                    AudioManager.Instance.PlayAudio("clap");
                    winPanel.ShowMenu();
                }, 2);
            }
        }

        public void OnWrongAnswer()
        {
            AudioManager.Instance.PlayAudio("wrong");
        }
    }
}