﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AnimalWordGame
{
    public class Word : MonoBehaviour
    {
        public string word;

        [SerializeField] private GameObject correctParticle;
        [SerializeField] private GameObject wrongParticle;

        private DraggableItem draggableItem;

        private void Awake()
        {
            draggableItem = GetComponent<DraggableItem>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("AnimalBox"))
            {
                var animalComponent = other.GetComponent<Animal>();

                if (word == animalComponent.animalName)
                {
                    GameManager.Instance.OnCorrectAnswer();
                    Instantiate(correctParticle, animalComponent.transform.position, Quaternion.identity);
                    Destroy(gameObject);
                }
                else
                {
                    GameManager.Instance.OnWrongAnswer();
                    Instantiate(wrongParticle, animalComponent.transform.position, Quaternion.identity);
                    draggableItem.OnWrong();
                    Debug.Log("Wrong!");
                }
            }
        }
    }
}