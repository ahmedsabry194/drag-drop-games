﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;
using UnityEditor;

public class DraggableItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI word_TXT;

    private Vector3 initialPosition;
    private bool isDragging;

    private void Awake()
    {
        initialPosition = transform.position;
    }

    public void FillWord(string word)
    {
        word_TXT.text = ArabicFixerTool.FixLine(word);
    }

    private void OnMouseDown()
    {
        isDragging = true;
    }

    private void OnMouseDrag()
    {
        if (isDragging)
        {
            Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            transform.position = mousePosition;
        }
    }

    private void OnMouseUp()
    {
        OnWrong();
    }
    
    public void OnWrong()
    {
        isDragging = false;
        transform.DOMove(initialPosition, 0.5f);
    }
}