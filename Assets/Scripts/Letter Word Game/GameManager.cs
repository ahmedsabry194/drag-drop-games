﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LetterWordGame
{
    public class GameManager : MonoBehaviour
    {
        public int requiredCorrectAnswers = 3;

        [SerializeField] private List<DraggableItem> DraggableItems;
        [SerializeField] private AiryUIAnimationManager winPanel;
        [SerializeField] private GameObject target;
        [SerializeField] private GameObject correctParticle;
        [SerializeField] private GameObject wrongParticle;

        private List<DraggableItem> correctDraggableItems = new List<DraggableItem>();
        private List<DraggableItem> falseDraggableItems = new List<DraggableItem>();
        private int correctAnswers = 0;

        public static GameManager Instance;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            PickRandomDraggableItems();
        }

        private void Start()
        {
        }

        private void PickRandomDraggableItems()
        {
            List<int> indexes = new List<int>();

            for (int i = 0; i < 3; i++)
            {
                int index;

                do
                {
                    index = Random.Range(0, DraggableItems.Count);
                } while (indexes.Contains(index));

                indexes.Add(index);
                correctDraggableItems.Add(DraggableItems[index]);
            }

            foreach (var item in DraggableItems)
            {
                if (!correctDraggableItems.Contains(item))
                    falseDraggableItems.Add(item);
            }
        }

        public void FillCorrectWords(string[] words)
        {
            for (int i = 0; i < correctDraggableItems.Count; i++)
            {
                correctDraggableItems[i].FillWord(words[i]);
            }
        }

        public void FillFalseWords(List<Letter> letters)
        {
            if (falseDraggableItems.Count != letters.Count)
            {
                Debug.LogError("Not the same count!");
                return;
            }

            for (int i = 0; i < falseDraggableItems.Count; i++)
            {
                falseDraggableItems[i].FillWord(letters[i].Words[Random.Range(0, letters[i].Words.Length)]);
            }
        }

        public bool CheckAnswer(DraggableItem draggableItem)
        {
            return (correctDraggableItems.Contains(draggableItem));
        }

        public void OnCorrectAnswer()
        {
            AudioManager.Instance.PlayAudio("correct");
            Instantiate(correctParticle, target.transform.position, Quaternion.identity);

            correctAnswers++;

            if (correctAnswers >= requiredCorrectAnswers)
            {
                OnWin();
            }
        }

        public void OnWrongAnswer()
        {
            AudioManager.Instance.PlayAudio("wrong");
            Instantiate(wrongParticle, target.transform.position, Quaternion.identity);
        }

        private void OnWin()
        {
            Destroy(LetterSelector.Instance.gameObject);

            CoroutineHandler.Instance.PlayCoroutineWithTime(() =>
            {
                winPanel.ShowMenu();
                AudioManager.Instance.PlayAudio("clap");
                foreach (var item in falseDraggableItems)
                {
                    Destroy(item);
                }
            }, 2);
        }
    }
}