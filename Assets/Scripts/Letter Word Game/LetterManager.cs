﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LetterWordGame
{
    public class LetterManager : MonoBehaviour
    {
        public List<Letter> Letters = new List<Letter>();

        [SerializeField] private GameObject winPanel;
        [SerializeField] private GameObject winParticlePrefab;
        [SerializeField] private Image correctAnswersIndicator;
        [SerializeField] private Text letter_TXT;

        private List<Letter> FalseLetters = new List<Letter>();

        public static LetterManager Instance;

        private void Awake()
        {
            if (Instance == null)
                Instance = this;
            else
                Destroy(gameObject);
        }

        private void Start()
        {
            SetFiveFalseLetters();

            GameManager.Instance.FillCorrectWords(Letters.Find(l => l.LetterChar == LetterSelector.Instance.selectedLetter).Words);
        }

        private void SetFiveFalseLetters()
        {
            var falseLetters = new List<Letter>();

            for (int i = 0; i < 5; i++)
            {
                Letter let;

                do
                {
                    int rnd = Random.Range(1, Letters.Count);
                    let = Letters[rnd];
                } while (let.LetterChar == LetterSelector.Instance.selectedLetter);

                falseLetters.Add(let);
            }

            FalseLetters = falseLetters;
            GameManager.Instance.FillFalseWords(FalseLetters);
        }
    }

    [System.Serializable]
    public class Letter
    {
        public string LetterName;
        public string LetterChar;
        public string[] Words = new string[3];
    }
}