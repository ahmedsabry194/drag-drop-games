﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LetterSelector : MonoBehaviour
{
    public string selectedLetter;
    public string[] allLetters;

    public static LetterSelector Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SelectLetter(string letter)
    {
        this.selectedLetter = letter;
    }
}