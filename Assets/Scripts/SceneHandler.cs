﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{
    public void LoadLetterWordGame()
    {
        SceneManager.LoadScene("1 - LetterWordGame");
    }

    public void LoadAnimalWordGame()
    {
        SceneManager.LoadScene("2 - AnimalWordGame");
    }

    public void LoadTrueFalseGame()
    {
        SceneManager.LoadScene("3 - TrueFalseGame");
    }

    public void LoadWordAssembleGame()
    {
        SceneManager.LoadScene("4 - WordAssembleGame");
    }

    public void GoToHome()
    {
        SceneManager.LoadScene("0 - Home");
    }

    public void LoadSceneByName(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
}