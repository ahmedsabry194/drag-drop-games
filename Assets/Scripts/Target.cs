﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace LetterWordGame
{
    public class Target : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI letter_TXT;

        private void Start()
        {
            letter_TXT.text = LetterSelector.Instance.selectedLetter;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("WordBox"))
            {
                if (GameManager.Instance.CheckAnswer(other.GetComponent<DraggableItem>()))
                {
                    GameManager.Instance.OnCorrectAnswer();
                    Destroy(other.gameObject);
                }
                else
                {
                    GameManager.Instance.OnWrongAnswer();
                    other.GetComponent<DraggableItem>().OnWrong();
                }
            }
        }
    }
}