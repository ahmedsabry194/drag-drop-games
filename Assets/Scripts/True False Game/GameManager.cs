﻿using LetterWordGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TrueFalseGame
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private AiryUIAnimationManager winPanel;
        [SerializeField] private Target target;
        [SerializeField] private Letter[] letters;
        [SerializeField] private GameObject correctParticle;
        [SerializeField] private GameObject wrongParticle;
        [SerializeField] private Text quizLetter_TXT;

        private int requiredAnswers = 3;
        private int currentCorrectAnswers = 0;

        public static GameManager Instance;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {
            target.targetLetter = LetterSelector.Instance.selectedLetter;
            quizLetter_TXT.text = LetterSelector.Instance.selectedLetter;
            FillLetters();
        }

        public bool CheckAnswer(Letter letter)
        {
            return (letter.letter == target.targetLetter);
        }

        public void OnCorrectAnswer()
        {
            currentCorrectAnswers++;
            AudioManager.Instance.PlayAudio("correct");
            //Instantiate(correctParticle, target.transform.position, Quaternion.identity);

            if (currentCorrectAnswers >= 3)
            {
                OnWin();
            }
        }

        public void OnWrongAnswer()
        {
            AudioManager.Instance.PlayAudio("wrong");
            Instantiate(wrongParticle, target.transform.position, Quaternion.identity);
        }

        private void OnWin()
        {
            Destroy(LetterSelector.Instance.gameObject);

            CoroutineHandler.Instance.PlayCoroutineWithTime(() =>
            {
                AudioManager.Instance.PlayAudio("clap");
                winPanel.ShowMenu();
            }, 2);
        }

        private void FillLetters()
        {
            List<int> Xs = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                int x;
                do
                {
                    x = Random.Range(0, letters.Length);
                } while (Xs.Contains(x));

                Xs.Add(x);
                letters[x].FillLetter(LetterSelector.Instance.selectedLetter);
            }

            for (int i = 0; i < letters.Length; i++)
            {
                if (Xs.Contains(i))
                    continue;

                string let;
                do
                {
                    let = LetterSelector.Instance.allLetters[Random.Range(0, LetterSelector.Instance.allLetters.Length)];
                } while (let == LetterSelector.Instance.selectedLetter);

                letters[i].FillLetter(let);
            }
        }
    }
}