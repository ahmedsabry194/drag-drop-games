﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace TrueFalseGame
{
    public class Letter : MonoBehaviour
    {
        //[SerializeField] private TextMeshPro letter_TXT;
        
            [SerializeField] private TextMeshProUGUI letter_TXT;
        public string letter;

        public void FillLetter(string letter)
        {
            this.letter = letter;
            letter_TXT.text = letter;
        }
    }
}