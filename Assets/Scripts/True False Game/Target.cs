﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrueFalseGame
{
    public class Target : MonoBehaviour
    {
        public string targetLetter;

        [SerializeField] private Animator animator;
        [SerializeField] private ParticleSystem openEffect;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("WordBox"))
            {
                if (GameManager.Instance.CheckAnswer(other.GetComponent<Letter>()))
                {
                    animator.SetTrigger("Open");
                    GameManager.Instance.OnCorrectAnswer();
                    Destroy(other.gameObject);
                }
                else
                {
                    animator.SetTrigger("Shut");
                    GameManager.Instance.OnWrongAnswer();
                    other.GetComponent<DraggableItem>().OnWrong();
                }
            }
        }

        public void ShowOpenEffect()
        {
            openEffect.Play(true);
        }
    }
}