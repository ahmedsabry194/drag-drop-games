﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace WordAssembleGame
{
    public class DraggableLetter : MonoBehaviour
    {
        public string letter;

        [SerializeField] private TextMeshProUGUI letter_TXT;

        public void SetLetterText(string text)
        {
            letter = text;
            letter_TXT.text = text;
        }
    }
}