﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace WordAssembleGame
{
    public class GameManager : MonoBehaviour
    {
        public List<string> words;

        [SerializeField] private TextMeshProUGUI fullWord_TXT;
        [SerializeField] private List<TargetLetter> correctLettersTexts;
        [SerializeField] private List<DraggableLetter> randomizedLettersTexts;
        [SerializeField] AiryUIAnimationManager fullWordPanel;
        [SerializeField] AiryUIAnimationManager winPanel;
        [SerializeField] private GameObject correctParticle;
        [SerializeField] private GameObject wrongParticle;

        private string taretWord;
        private string[] letters = new string[3];
        private int correctAnswers = 0;

        public static GameManager Instance;

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
            }
            else
            {
                Destroy(gameObject);
            }

            taretWord = words[Random.Range(0, words.Count)];
            fullWord_TXT.text = ArabicFixerTool.FixLine(taretWord);

            letters[0] = taretWord[0].ToString();
            letters[1] = taretWord[1].ToString();
            letters[2] = taretWord[2].ToString();

            Shuffle();

            for (int i = 0; i < letters.Length; i++)
            {
                correctLettersTexts[i].SetLetterText(letters[i]);
            }

            for (int i = 0; i < ints.Count; i++)
            {
                randomizedLettersTexts[i].SetLetterText(letters[ints[i]]);
            }
        }

        public List<int> ints = new List<int>() { 0, 1, 2 };

        public void Shuffle()
        {
            int n = ints.Count;
            while (n > 1)
            {
                n--;
                int k = Random.Range(0, ints.Count);
                int value = ints[k];
                ints[k] = ints[n];
                ints[n] = value;
            }
        }

        public bool CheckAnswer(string letter1, string letter2)
        {
            return (letter1 == letter2);
        }

        public void OnCorrectAnswer(Vector3 position)
        {
            correctAnswers++;
            AudioManager.Instance.PlayAudio("correct");
            Instantiate(correctParticle, position, Quaternion.identity);

            if (correctAnswers >= 3)
            {
                OnWin();
            }
        }

        public void OnWrongAnswer(Vector3 position)
        {
            Debug.Log("Wrong!");
            Instantiate(wrongParticle, position, Quaternion.identity);
        }

        private void OnWin()
        {
            Destroy(LetterSelector.Instance.gameObject);

            CoroutineHandler.Instance.PlayCoroutineWithTime(() =>
            {
                fullWordPanel.ShowMenu();
            }, 1);

            CoroutineHandler.Instance.PlayCoroutineWithTime(() =>
            {
                AudioManager.Instance.PlayAudio("clap");
                winPanel.ShowMenu();
            }, 5);
        }
    }
}