﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace WordAssembleGame
{
    public class TargetLetter : MonoBehaviour
    {
        [SerializeField] private string letter;

        [SerializeField] private TextMeshProUGUI letter_TXT;

        public void SetLetterText(string text)
        {
            letter = text;
            letter_TXT.text = text;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag("WordBox"))
            {
                var letter = other.GetComponent<DraggableLetter>();

                if (GameManager.Instance.CheckAnswer(this.letter, letter.letter))
                {
                    GameManager.Instance.OnCorrectAnswer(transform.position);
                    Destroy(other.gameObject);
                }
                else
                {
                    GameManager.Instance.OnWrongAnswer(transform.position);
                    other.GetComponent<DraggableItem>().OnWrong();
                }
            }
        }
    }
}